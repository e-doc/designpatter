package br.padroes.naoEstudado.templateMethod;

public enum ModoDeReproducao {
	porNome, porAutor, porAno, porEstrela
}
