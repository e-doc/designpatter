package br.padroes.naoEstudado.iteratorInterno;

import br.padroes.naoEstudado.iteratorExterno.Canal;
import br.padroes.naoEstudado.iteratorExterno.IteradorInterface;

public abstract class IteradorInterno {

	IteradorInterface it;

	public void percorrerLista() {
		for (it.first(); !it.isDone(); it.next()) {
			operacao(it.currentItem());
		}
	}

	protected abstract void operacao(Canal canal);
}
