package br.padroes.naoEstudado.iteratorInterno;

import br.padroes.naoEstudado.iteratorExterno.Canal;
import br.padroes.naoEstudado.iteratorExterno.IteradorInterface;

public class IteradorPrint extends IteradorInterno {

	public IteradorPrint(IteradorInterface it) {
		this.it = it;
	}

	@Override
	protected void operacao(Canal canal) {
		System.out.println(canal.nome);
	}

}
