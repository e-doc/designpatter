package br.padroes.naoEstudado.strategy;

interface CalculaImposto {
	double calculaSalarioComImposto(Funcionario umFuncionario);
}
