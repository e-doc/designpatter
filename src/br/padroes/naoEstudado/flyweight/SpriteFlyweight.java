package br.padroes.naoEstudado.flyweight;

public abstract class SpriteFlyweight {
	public abstract void desenharImagem(Ponto ponto);
}
