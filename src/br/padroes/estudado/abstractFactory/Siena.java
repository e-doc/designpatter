package br.padroes.estudado.abstractFactory;

public class Siena implements CarroSedan {

	@Override
	public void exibirInfoSedan() {
		System.out.println("Modelo: Siena\nF�brica: Fiat\nCategoria:Sedan");
	}

}
