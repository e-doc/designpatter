package br.padroes.estudado.abstractFactory;

public interface FabricaDeCarro {
	CarroSedan criarCarroSedan();
	CarroPopular criarCarroPopular();
}
